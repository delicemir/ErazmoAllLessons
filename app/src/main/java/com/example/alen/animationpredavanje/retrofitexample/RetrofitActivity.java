package com.example.alen.animationpredavanje.retrofitexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alen.animationpredavanje.R;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EActivity(R.layout.activity_retrofit)
public class RetrofitActivity extends AppCompatActivity {

    @ViewById(resName = "search_edit_text")
    protected EditText searchEditText;
    @ViewById(resName = "no_movie_holder")
    protected RelativeLayout noMovieHolder;
    @ViewById(resName = "movie_holder")
    protected RelativeLayout movieHolder;
    @ViewById(resName = "movie_image")
    protected ImageView movieImage;
    @ViewById(resName = "movie_title")
    protected TextView movieTitle;
    @ViewById(resName = "movie_year")
    protected TextView movieYear;
    @ViewById(resName = "movie_genre")
    protected TextView movieGenre;
    @ViewById(resName = "rating")
    protected RatingBar rating;
    @ViewById(resName = "movie_rating")
    protected TextView movieRating;
    @ViewById(resName = "movie_plot")
    protected TextView moviePlot;
    @ViewById(resName = "progress")
    protected ProgressBar progressBar;
    @ViewById(resName = "movie_details_holder")
    protected RelativeLayout movieDetailsHolder;
    @ViewById(resName = "searched_list")
    protected RecyclerView searchedList;

    MovieAdapter movieAdapter;
    MovieAdapter.OnMovieClickLister lister;
    int pageNumber = 2;

    @AfterViews
    public void init() {

        final GridLayoutManager glm = new GridLayoutManager(getApplicationContext(), 3);
        searchedList.setLayoutManager(glm);

        lister = new MovieAdapter.OnMovieClickLister() {
            @Override
            public void onMovieClick(String title) {
                movieDetailsHolder.setVisibility(View.VISIBLE);
                onMovieClicked(title);
            }
        };


        RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = glm.getChildCount();
                int totalItemCount = glm.getItemCount();
                int pastVisibleItems = glm.findFirstVisibleItemPosition();
                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                    paginationListSearch(searchEditText.getText().toString(), pageNumber);
                    pageNumber++;
                }
            }
        };
        searchedList.setOnScrollListener(mScrollListener);
    }

    @Click(resName = "movie_image")
    public void closeDetails() {
        movieDetailsHolder.setVisibility(View.GONE);
    }

    @Click(resName = "search_btn")
    public void searchMovieBtn() {
        String title = searchEditText.getText().toString();

        if (title.isEmpty()) {
            return;
        }
        Call<SearchedMovieModel> call = ServiceGenerator.createService(MoviesProviderClient.class).getSearchedMovie(title, 1);
        call.enqueue(new Callback<SearchedMovieModel>() {
            @Override
            public void onResponse(Call<SearchedMovieModel> call, Response<SearchedMovieModel> response) {
                if (response == null || response.body() == null) {
                    return;
                }
                List<SearchedMovieModel.Search> list = response.body().Search;
                movieAdapter = new MovieAdapter(list, getApplicationContext(), lister);
                searchedList.setAdapter(movieAdapter);
                movieAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<SearchedMovieModel> call, Throwable t) {

            }
        });

    }

    public void paginationListSearch(String title, int pageNumber) {
        Call<SearchedMovieModel> call = ServiceGenerator.createService(MoviesProviderClient.class).getSearchedMovie(title, pageNumber);
        call.enqueue(new Callback<SearchedMovieModel>() {
            @Override
            public void onResponse(Call<SearchedMovieModel> call, Response<SearchedMovieModel> response) {
                if (response == null || response.body() == null) {
                    return;
                }
                List<SearchedMovieModel.Search> list = response.body().Search;

                movieAdapter.addToList(list);
            }

            @Override
            public void onFailure(Call<SearchedMovieModel> call, Throwable t) {

            }
        });
    }

    public void onMovieClicked(String title) {
        if (title.isEmpty()) {
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        Call<MovieModel> call = ServiceGenerator.createService(MoviesProviderClient.class).getMovie(title);
        call.enqueue(new Callback<MovieModel>() {
            @Override
            public void onResponse(Call<MovieModel> call, Response<MovieModel> response) {
                if (response == null || response.body() == null) {
                    return;
                }
                progressBar.setVisibility(View.GONE);
                noMovieHolder.setVisibility(View.GONE);
                movieHolder.setVisibility(View.VISIBLE);
                MovieModel model = response.body();

                if (model.Poster != null) {
                    Picasso.with(getApplicationContext()).load(model.Poster).error(R.drawable.movie).into(movieImage);
                }
                if (model.Title != null) {
                    movieTitle.setText(model.Title);
                }
                String genre = "";
                if (model.Runtime != null) {
                    genre += model.Runtime;
                }
                if (model.Genre != null) {
                    genre += " | " + model.Genre;
                }
                if (model.Released != null) {
                    genre += " | " + model.Released;
                }
                movieGenre.setText(genre);

                if (model.Year != null) {
                    movieYear.setText("(" + model.Year + ")");
                }
                if (model.imdbRating != null) {
                    movieRating.setText(String.valueOf(model.imdbRating));
                    rating.setMax(5);
                    rating.setRating(model.imdbRating / 2);
                }
                if (model.Plot != null) {
                    moviePlot.setText(model.Plot);
                }

            }

            @Override
            public void onFailure(Call<MovieModel> call, Throwable t) {
                noMovieHolder.setVisibility(View.VISIBLE);
                movieHolder.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
