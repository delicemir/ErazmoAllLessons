package com.example.alen.animationpredavanje;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class ProcessingJSON extends AppCompatActivity {

    EditText cityName;
    TextView cityNameResult;
    TextView descriptionResult;
    TextView currentTimeResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_processing_json);

        cityName = (EditText) findViewById(R.id.city_name);
        cityNameResult = (TextView) findViewById(R.id.result_city_name) ;
        descriptionResult = (TextView) findViewById(R.id.result_description);
        currentTimeResult = (TextView) findViewById(R.id.result_current_time);
    }

    public void calculateTime(View view) {
        DownloadClass task = new DownloadClass();

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(cityName.getWindowToken(), 0);
        try {
            task.execute("http://api.openweathermap.org/data/2.5/weather?q=" + cityName.getText().toString() + "&APPID=bd5e378503939ddaee76f12ad7a97608").get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public class DownloadClass extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            String result = "";
            URL url;
            HttpURLConnection httpURLConnection = null;
            try {
                url = new URL(urls[0]);

                httpURLConnection = (HttpURLConnection) url.openConnection();

                InputStream inputStream = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int data = inputStreamReader.read();

                while (data != -1) {
                    char character = (char) data;
                    result += character;

                    data = inputStreamReader.read();
                }

                return result;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject object = new JSONObject(s);

                String weather = object.getString("weather");
                JSONArray jsonArray = new JSONArray(weather);

                for(int i = 0; i < 1; i++) {

                    JSONObject jsonPart = jsonArray.getJSONObject(i);

                    cityNameResult.setText(object.getString("name"));
                    descriptionResult.setText(jsonPart.getString("description"));
                    currentTimeResult.setText(jsonPart.getString("main"));
                }
            } catch (Exception e) {
                Toast.makeText(ProcessingJSON.this, "Vrijeme je nedostupno", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
