package com.example.alen.animationpredavanje.recycleradapterlistener;

public class CarItem {

    public String title;
    public String imageUrl;
    public String description;
    public String email;

    public CarItem(String title, String imageUrl, String description, String email) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.description = description;
        this.email = email;
    }
}
