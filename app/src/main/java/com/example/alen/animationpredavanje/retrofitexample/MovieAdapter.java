package com.example.alen.animationpredavanje.retrofitexample;

import android.content.Context;
import android.graphics.Movie;
import android.hardware.Camera;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.alen.animationpredavanje.R;
import com.example.alen.animationpredavanje.recycleradapterlistener.RecyclerCustomAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder>{

    List<SearchedMovieModel.Search> list = new ArrayList<>();
    Context context = null;
    OnMovieClickLister lister;

    public MovieAdapter(List<SearchedMovieModel.Search> list, Context context, OnMovieClickLister lister) {
        this.list = list;
        this.context = context;
        this.lister = lister;
    }

    public void addToList(List<SearchedMovieModel.Search> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new MovieAdapter.ViewHolder(view, lister);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SearchedMovieModel.Search object = list.get(position);

        Picasso.with(context).load(object.Poster).error(R.drawable.movie).into(holder.imageView);
        holder.title = object.Title;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        String title;
        ImageView imageView;

        public ViewHolder(View itemView, final OnMovieClickLister lister) {
            super(itemView);

            imageView = itemView.findViewById(R.id.movie_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lister.onMovieClick(title);
                }
            });
        }
    }

    public interface OnMovieClickLister {
        void onMovieClick(String title);
    }
}
