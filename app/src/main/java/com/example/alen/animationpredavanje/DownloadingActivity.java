package com.example.alen.animationpredavanje;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class DownloadingActivity extends AppCompatActivity {

    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloading);

        image = (ImageView) findViewById(R.id.downloaded_image);

        ContentDownloader contentDownloader = new ContentDownloader();

        String result = "";
        try {
            result = contentDownloader.execute("http://www.tanjir.ba/").get();
        } catch (InterruptedException | ExecutionException e) {
            result = "BLOCKED";
            e.printStackTrace();
        }

        Log.v("RESULT", result);

/*        Downloader downloader = new Downloader();
        try {
            downloader.onProgressUpdate(10);
            Log.v("TAG", downloader.execute("http://www.tanjir.ba/tuzla/zmaj-od-bosne").get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }*/
    }

    public void downloadImage(View view) {

        Picasso.with(this)
                .load("https://familyguyaddicts.files.wordpress.com/2014/04/peter-animation-033idlepic4x.png")
                .placeholder(R.drawable.placeholdere)
                .error(R.drawable.error)
                .into(image);

/*        ImageDownloader task = new ImageDownloader();
        Bitmap myImage = null;

        try {
            myImage = task.execute("https://familyguyaddicts.files.wordpress.com/2014/04/peter-animation-033idlepic4x.png").get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        if (myImage != null) {
            image.setImageBitmap(myImage);
        }*/
    }

    public class ContentDownloader extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream inputStream = urlConnection.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();

                String singleLine;

                while ((singleLine = r.readLine()) != null) {
                    result.append(singleLine).append('\n');
                }

                return result.toString();
            } catch (IOException e) {
                e.printStackTrace();
                return "Bad request";
            }
        }
    }

    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                InputStream in = connection.getInputStream();
                return  BitmapFactory.decodeStream(in);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

/*    public class Downloader extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = urlConnection.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder total = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line).append('\n');
                }
                return total.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }

        protected void onProgressUpdate(Integer... progress) {
            Log.v("PROGRESS", String.valueOf(progress[0]));
        }
    }*/

/*    public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream inputStream = urlConnection.getInputStream();

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                return bitmap;

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }*/
}
