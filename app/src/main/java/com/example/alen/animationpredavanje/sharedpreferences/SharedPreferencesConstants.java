package com.example.alen.animationpredavanje.sharedpreferences;

public class SharedPreferencesConstants {

    public static final String SHARED_PREFERENCES = "com.example.alen.animationpredavanje.sharedpreferences";
    public static final String SHARED_USERNAME = "username";
    public static final String SHARED_LIST = "list";
}
