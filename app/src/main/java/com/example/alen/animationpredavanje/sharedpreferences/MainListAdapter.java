package com.example.alen.animationpredavanje.sharedpreferences;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alen.animationpredavanje.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.ViewHolder> {

    List<NewsObject> mainList = new ArrayList<>();

    public MainListAdapter(List<NewsObject> mainList) {
        this.mainList = mainList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NewsObject newsObject = mainList.get(position);

        holder.title.setText(newsObject.getTitle());
        holder.description.setText(newsObject.getDescription());
        Picasso.with(holder.context).load(newsObject.getImageUrl()).into(holder.image);
        holder.imageUrl = newsObject.getImageUrl();
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView title;
        public TextView description;
        Context context;
        String imageUrl;

        public ViewHolder(final View itemView) {
            super(itemView);

            this.context = itemView.getContext();

            image = (ImageView) itemView.findViewById(R.id.list_image);
            title = (TextView) itemView.findViewById(R.id.list_title);
            description = (TextView) itemView.findViewById(R.id.list_description);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, NewsActivity.class);
                    i.putExtra("title", title.getText().toString());
                    i.putExtra("image", imageUrl);

                    context.startActivity(i);
                }
            });
        }
    }
}
