package com.example.alen.animationpredavanje.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "CarsDatabase";

    // Car database table
    private static final String TABLE_CARS = "cars";

    // Car columns
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_IS_NEW = "isNew";
    private static final String COLUMN_AGE = "age";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_TABLES = "CREATE TABLE " + TABLE_CARS + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY," + COLUMN_NAME + " TEXT,"
                + COLUMN_IS_NEW + " BOOLEAN," + COLUMN_AGE + " INTEGER" + ")";
        sqLiteDatabase.execSQL(CREATE_TABLES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CARS);

        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public void addCar(CarObject car) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, car.getName());
        values.put(COLUMN_IS_NEW, car.isNew());
        values.put(COLUMN_AGE, car.getAge());

        // Inserting Row
        db.insert(TABLE_CARS, null, values);
        db.close(); // Closing database connection
    }

    public List<CarObject> getCars() {
        List<CarObject> cars = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CARS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                CarObject car = new CarObject();
                car.setId(cursor.getInt(0));
                car.setName(cursor.getString(1));
                car.setNew(Boolean.valueOf(cursor.getString(2)));
                car.setAge(cursor.getInt(3));

                cars.add(car);
            } while (cursor.moveToNext());
        }

        return cars;
    }
}
