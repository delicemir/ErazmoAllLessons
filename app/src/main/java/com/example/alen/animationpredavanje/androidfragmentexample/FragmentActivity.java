package com.example.alen.animationpredavanje.androidfragmentexample;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.alen.animationpredavanje.R;
import com.example.alen.animationpredavanje.androidfragmentexample.scrolabletabs.ScrollableTabsActivity;
import com.example.alen.animationpredavanje.androidfragmentexample.tabs.TabIconActivity;
import com.example.alen.animationpredavanje.androidfragmentexample.tabs.TabIconAdapter;
import com.example.alen.animationpredavanje.androidfragmentexample.viewpageradapter.ViewPagerActivity;

public class FragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }

    public void goToExampleWithViewPagerAdapter(View view) {
        Intent i = new Intent(FragmentActivity.this, ViewPagerActivity.class);
        startActivity(i);
    }

    public void goToExampleWithTabLayoutText(View view) {
        Intent i = new Intent(FragmentActivity.this, TabIconActivity.class);
        startActivity(i);
    }

    public void goToExampleWithCustomTabLayout(View view) {
        Intent i = new Intent(FragmentActivity.this, ScrollableTabsActivity.class);
        startActivity(i);
    }
}
