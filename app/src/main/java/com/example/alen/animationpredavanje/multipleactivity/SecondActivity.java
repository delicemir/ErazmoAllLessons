package com.example.alen.animationpredavanje.multipleactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.alen.animationpredavanje.R;

public class SecondActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textView = (TextView) findViewById(R.id.second_activity_text);

        Intent intent = getIntent();

        String data = intent.getStringExtra("dta");

        textView.setText(data);
    }
}
