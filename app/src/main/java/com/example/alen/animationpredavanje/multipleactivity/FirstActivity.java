package com.example.alen.animationpredavanje.multipleactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.alen.animationpredavanje.R;

public class FirstActivity extends AppCompatActivity {

    EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);

        editText = (EditText) findViewById(R.id.tex_id);
    }

    public void goToSecondActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), SecondActivity.class);

        String text = editText.getText().toString();

        intent.putExtra("data", text);

        startActivity(intent);
    }
}
