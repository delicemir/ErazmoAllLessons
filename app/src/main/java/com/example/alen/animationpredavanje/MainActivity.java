package com.example.alen.animationpredavanje;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    SeekBar volumeControl;
    SeekBar progressControl;

    MediaPlayer media;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        volumeControl = (SeekBar) findViewById(R.id.volume_seek);
        progressControl = (SeekBar) findViewById(R.id.progress_seek);
        media = MediaPlayer.create(this, R.raw.song);

        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int max = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int curr = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeControl.setMax(max);
        volumeControl.setProgress(curr);

        volumeControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, i, 0);
                Log.v("TAG", "onProgressChanged");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.v("TAG", "onStart");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.v("TAG", "onStop");
            }
        });

        progressControl.setMax(media.getDuration());

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.v("TAG", "timerTick");
                progressControl.setProgress(media.getCurrentPosition());
            }
        }, 0, 500);

        progressControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                media.seekTo(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void playSong(View view) {
        media.start();
    }

    public void stopSong(View view) {
        media.pause();
    }
}
