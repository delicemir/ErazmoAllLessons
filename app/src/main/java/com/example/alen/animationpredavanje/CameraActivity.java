package com.example.alen.animationpredavanje;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;

import java.util.ArrayList;

public class CameraActivity extends AppCompatActivity {

    private ImageView imageView ;

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int READ_STORAGE_CODE = 1001;
    private static final int WRITE_STORAGE_CODE = 1002;
    private ArrayList<String> pathList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        imageView = findViewById(R.id.image_camera);

    }

    public void captureCamera(View view) {
        if(!hasCamera()) {
            return;
        }

        launchCamera();
    }

    public boolean hasCamera() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }

    public void launchCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,REQUEST_IMAGE_CAPTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

        if(resultCode != RESULT_OK) {
            return;
        }

        if(requestCode == REQUEST_IMAGE_CAPTURE) {
            Bundle extra = data.getExtras();
            if(extra.get("data") instanceof Bitmap) {
                Bitmap photo = (Bitmap) extra.get("data");
                imageView.setImageBitmap(photo);
            }
        }
        else if(requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            this.pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                StringBuilder sb=new StringBuilder("");
                for(int i=0;i<pathList.size();i++) {
                    sb.append("Photo"+(i+1)+":"+pathList.get(i));
                    sb.append("\n");
                    System.out.println("Image path: "+pathList.get(i));
                }
                Bitmap photo = BitmapFactory.decodeFile(pathList.get(0));
                imageView.setImageBitmap(photo);
            }
        }
    }

    public void takeImage(View view) {

        openImagePickerIntent();
    }

    private void openImagePickerIntent() {

        if(isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent mIntent = new Intent(this, PickImageActivity.class);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 60);
            mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
            startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
        }
        else {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, READ_STORAGE_CODE);
        }

    }

    private boolean isPermissionGranted (String permission) {
        int result = ContextCompat.checkSelfPermission(this,permission);
        if(result == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermission(String permission, int code) {

        if(ActivityCompat.shouldShowRequestPermissionRationale(this,permission)) {

        }

        ActivityCompat.requestPermissions(this,new String[] {permission},code);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == READ_STORAGE_CODE) {
            if(grantResults.length >0 && grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                openImagePickerIntent();
            }
            else {
                CameraActivity.this.finish();
            }

        }
        else if(requestCode == WRITE_STORAGE_CODE) {

            if(grantResults.length >0 && grantResults[0]== PackageManager.PERMISSION_GRANTED) {

            }
            else {
                CameraActivity.this.finish();
            }

        }

    }
}
