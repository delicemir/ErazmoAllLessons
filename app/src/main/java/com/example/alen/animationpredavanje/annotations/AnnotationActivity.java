package com.example.alen.animationpredavanje.annotations;

import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.alen.animationpredavanje.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_annotation)
public class AnnotationActivity extends AppCompatActivity {

    @ViewById(R.id.text_annotations_id)
    protected TextView textView;


    @AfterViews
    public void initialize() {
        //setTextWithDelay();
    }

    @UiThread(delay = 10000)
    public void setTextWithDelay() {
        textView.setText("/// with delay");
    }

    @Click(resName = "click_me_btn")
    public void clickMe() {
        setTextWithDelay();
    }
}
