package com.example.alen.animationpredavanje.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.alen.animationpredavanje.R;

import java.util.List;

public class DatabaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);

        createDatabaseFromHelper();
    }

    public void createDatabaseFromHelper() {
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        setValuesToDatabase(databaseHelper);

        List<CarObject> cars = getAllCars(databaseHelper);

        for (CarObject car : cars) {
            Log.v("TAG", String.valueOf(car.getId()));
            Log.v("TAG", car.getName());
            Log.v("TAG", String.valueOf(car.getAge()));
            Log.v("TAG", String.valueOf(car.isNew()));
            Log.v("TAG","-----------");
        }

    }

    public List<CarObject> getAllCars(DatabaseHelper database) {
        return database.getCars();
    }

    public void setValuesToDatabase(DatabaseHelper database) {
        CarObject object1 = new CarObject();
        object1.setName("Golf");
        object1.setNew(false);
        object1.setAge(5);

        database.addCar(object1);

        CarObject object2 = new CarObject();
        object2.setName("Mecka");
        object2.setNew(true);
        object2.setAge(1);

        database.addCar(object2);

        CarObject object3 = new CarObject();
        object3.setName("Passat");
        object3.setNew(false);
        object3.setAge(10);

        database.addCar(object3);
    }

    public void createDatabase() {

        try {
            SQLiteDatabase database = this.openOrCreateDatabase("Users", MODE_PRIVATE, null);

            database.execSQL("CREATE TABLE IF NOT EXISTS users (name VARCHAR , age INT(3))");
            database.execSQL("INSERT INTO users (name, age) VALUES ('Alen', 25)");
            database.execSQL("INSERT INTO users (name, age) VALUES ('Semir', 24)");

            Cursor c = database.rawQuery("SELECT * FROM users", null);

            int nameIndex = c.getColumnIndex("name");
            int ageIndex = c.getColumnIndex("age");

            c.moveToFirst();

            while (c != null) {
                Log.v("name", c.getString(nameIndex));
                Log.v("age", String.valueOf(c.getInt(ageIndex)));

                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
