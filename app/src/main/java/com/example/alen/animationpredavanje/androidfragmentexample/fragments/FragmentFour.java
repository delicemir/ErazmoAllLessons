package com.example.alen.animationpredavanje.androidfragmentexample.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alen.animationpredavanje.R;

public class FragmentFour extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_four, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.v("Tag", "Fragment four attached");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.v("Tag", "Fragment four dettached");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v("Tag", "Fragment four destroyed");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v("Tag", "Fragment four pause");
    }


}
